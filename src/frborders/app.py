import os
import psycopg2
from flask import Flask, render_template

app = Flask(__name__)

# Obtention d'une connexion sur la base de données
def get_db_cnx():
  cnx = psycopg2.connect(host = os.getenv("APP_DB_HOST", "localhost"),
      database = os.getenv("APP_DB", "countries"),
      port = os.getenv("APP_DB_PORT", "5432"),
      user = os.getenv("APP_DB_USER"),
      password = os.getenv("APP_DB_PWD"))
  return cnx

# Accueil et unique page de l'application
@app.route('/')
def index():
  # Connexion à la base
  cnx = get_db_cnx()

  # Récupération de la liste des pays
  curs = cnx.cursor()
  curs.execute("SELECT alpha2,name,label FROM countries C, localisation L where C.loc=L.code order by label,name asc")
  countries = curs.fetchall()
  curs.close()
  
  # Fermeture de la connexion à la base
  cnx.close()
  
  # Affichage
  return render_template('index.html', countries=countries)
