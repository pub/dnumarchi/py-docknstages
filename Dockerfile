# -------------------------------------------------------------------
# Image de build pour les dépendances
# (Alpine possible si supportée par l'application, exemple généraliste ici)
# -------------------------------------------------------------------
FROM python:3.12-slim as builder

# Chemin (arbitraire) vers l'environnement virtuel Python
ARG VENV_PATH=/app/.venv
WORKDIR /app

# Les dépendances systèmes requises pour la compilation des
# dépendances python
RUN apt-get update && apt-get install -y --no-install-recommends \
    libc-dev libpq-dev gcc \
    && rm -rf /var/lib/apt/lists/*

# Installation Hatch
RUN pip install hatch 

# Création requirements.txt
COPY pyproject.toml .
RUN hatch dep show requirements -p > requirements.txt

# Création de l'environnement virtuel
RUN python -m venv --copies $VENV_PATH
ENV PATH=$VENV_PATH/bin:$PATH
RUN pip install --no-cache-dir -r requirements.txt

# -------------------------------------------------------------------
# Image de run
# -------------------------------------------------------------------
FROM python:3.12-slim as runner

ARG VENV_PATH=/app/.venv
WORKDIR /app

# Les dépendances systèmes requises pour l'exécution seule
RUN apt-get update && apt-get install -y --no-install-recommends \
    libpq5 \
    && rm -rf /var/lib/apt/lists/*

# Récupération des dépendances python précédemment construites
COPY --from=builder $VENV_PATH $VENV_PATH

# Copie de l'application
COPY src/frborders/ frborders/

# Démarrage
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PATH=$VENV_PATH/bin:$PATH

CMD ["gunicorn", "--bind", "0.0.0.0:8000", "frborders.wsgi:app"]
