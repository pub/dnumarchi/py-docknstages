#!/bin/sh
set -e

APPDEV_DB=countries
APPDEV_USER=countries
APPDEV_SCHEMA=countries

# Creation base et role
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE ROLE $APPDEV_USER LOGIN;
	CREATE DATABASE $APPDEV_DB;
EOSQL

# Creation schema
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$APPDEV_DB" <<-EOSQL
	CREATE SCHEMA $APPDEV_SCHEMA AUTHORIZATION $APPDEV_USER;
EOSQL

# Insertions des données
psql -v ON_ERROR_STOP=1 --username "$APPDEV_USER" --dbname "$APPDEV_DB" <<-EOSQL
  CREATE TABLE localisation(
      id SERIAL PRIMARY KEY,
      code VARCHAR(2) NOT NULL UNIQUE,
      label VARCHAR NOT NULL);
  INSERT INTO localisation(code, label) VALUES('OM', 'Outre-Mer');
  INSERT INTO localisation(code, label) VALUES('MT', 'Métropole');

  CREATE TABLE countries(
      id SERIAL PRIMARY KEY,
      name VARCHAR NOT NULL UNIQUE,
      alpha2 VARCHAR(2) NOT NULL UNIQUE,
      loc VARCHAR(3) NOT NULL);
  INSERT INTO countries(name, alpha2, loc) VALUES('Belgique', 'BE', 'MT');
  INSERT INTO countries(name, alpha2, loc) VALUES('Luxembourg', 'LU', 'MT');
  INSERT INTO countries(name, alpha2, loc) VALUES('Allemagne', 'DE', 'MT');
  INSERT INTO countries(name, alpha2, loc) VALUES('Suisse', 'CH', 'MT');
  INSERT INTO countries(name, alpha2, loc) VALUES('Italie', 'IT', 'MT');
  INSERT INTO countries(name, alpha2, loc) VALUES('Monaco', 'MC', 'MT');
  INSERT INTO countries(name, alpha2, loc) VALUES('Andorre', 'AD', 'MT');
  INSERT INTO countries(name, alpha2, loc) VALUES('Espagne', 'ES', 'MT');
  INSERT INTO countries(name, alpha2, loc) VALUES('Pays-Bas', 'NL', 'OM');
  INSERT INTO countries(name, alpha2, loc) VALUES('Suriname', 'SR', 'OM');
  INSERT INTO countries(name, alpha2, loc) VALUES('Bresil', 'BR', 'OM');
EOSQL
