# Démonstrateur Docker multi-stages Python

Démonstrateur de la construction [multi-stages](https://docs.docker.com/build/building/multi-stage/)
d'une image Python.

## Application de démonstration

Une application [Flask](https://flask.palletsprojects.com/) d'un seul écran listant les frontières terrestres de la France.<br>
Une [base de test](dbdev/Dockerfile) accompagne l'application.

L'intérêt est la dépendance [psycopg2](https://pypi.org/project/psycopg2/) qui demande une compilation de librairie native
et donc un environnement de compilation plus important que celui d'execution. Ce cas classique ce prête à une
construction Docker [multi-stages](https://docs.docker.com/build/building/multi-stage/).

L'application est construite par [Hatch](https://hatch.pypa.io/) (cf. [installation](https://hatch.pypa.io/latest/install/)).
Le [build multi-stages Docker](Dockerfile) convient également à un projet sur le seul et traditionnel `requirements.txt` (il suffit d'y retirer les quelques lignes dédiées à Hatch).<br>
La branche `alt/poetry` contient une variante basée sur [Poetry](https://python-poetry.org/).

Les principales commandes de construction et de démarrage sont reprises dans le fichier [Taskfile](Taskfile.yml)
(cf. [Task](https://taskfile.dev/)).

```shell
# Construction de l'image applicative
$ docker build dbdev/ -t frborders-dbdev:0.1.0

# Construction de l'image de test
$ docker build . -t frborders:0.1.0

# Démarrage de l'application
# Résultat sur http://localhost:8000
$ docker compose --env-file dev.env up -d
```

Pour tester l'application sans conteneur

* construisez une base de données Postgresl en vous inspirant du
[script d'initialisation](dbdev/docker-entrypoint-initdb.d/initdb.sh) de l'image de développement.
* Adaptez le [fichier d'environnement](dev.env) exemple.
* Lancez l'application depuis le dossier [src/](src/) : `hatch run gunicorn frborders.wsgi:app`.

## Construction multi-stages

Elle est illustrée dans le fichier [Dockerfile](Dockerfile). Elle permet de réduire significativement
la taille de l'image résultante en n'y embarquant pas les outils et librairies nécessaire à la
seule construction de l'image :

* Une étape (*stage*) de build.
  * Installation des outils pythons et librairies systèmes pour le téléchargement des dépendances et la compilation de psycopg2.
  * Création d'un environnement virtuel pour capturer les seules dépendances nécessaires.
  * Téléchargement des dépendances, compilation de psycopg2.
* Une étape (*stage*) de run
  * Installation des seuls outils et librairies nécessaires à l'execution de l'application.
  * Copie des dépendances capturées.
  * Copie de l'application.
